# Ḡ1SMS (Mobile SMS interface to Ḡ1 wallets): https://qo-op.com

Script de gestion de portefeuilles Ḡ1 par SMS sur téléphone portable.
Ces scripts sont rédigés en bash, sms_received.sh est exécuté par le démon gammu-smsd à chaque réception de message.
Il reconnait la commande reçue avant de lancer un sous process système qui exécute les actions sur le porte-monnaie grâce à l'appel de Silkaj en ligne de commande

## Configuration matérielle
- RaspberryPi3 
- Dongle 3G/USB
- DietPi (Raspbian Lite)
- gammu et smsd (https://tutoandco.colas-delmas.fr/software/envoyer-sms-gammu-deamon/)

On pourra également avec des réglages Firewall adéquat activer la connexion Internet par la clef 3G (si l'abonnement de votre carte SIM le permet) https://nicovddussen.wordpress.com/2014/11/12/setting-up-your-raspberry-pi-to-work-with-a-3g-dongle/

## Installer

	cd /root && git clone https://git.duniter.org/zicmama/G1SMS.git

## Configurer /etc/gammu-smsdrc

NB: Utiliser l'alias par id (/dev/serial/by-id/) de votre dongle plutot que le lien /dev/ttyUSBn dans la variable "device"

    # Configuration file for Gammu SMS Daemon
    # Gammu library configuration, see gammurc(5)
    [gammu]
    device = /dev/serial/by-id/usb-HUAWEI_Technologies_HUAWEI_Mobile-if00-port0
    name = Phone on USB serial port HUAWEI_Technology HUAWEI_Mobile
    connection = at19200
    synchronizetime = yes
    gammucoding = utf8
    #logformat = textall

    # SMSD configuration, see gammu-smsdrc(5)
    [smsd]
    service = files
    logfile = /tmp/smsd.log
    DeliveryReport = log

    # Increase for debugging information
    debuglevel = 0
    CommTimeout = 3
    PIN = 0000
    #CheckSecurity = 0

    # Soft reset every hour
    ResetFrequency = 3600
    # Disable (0), Enable HardReset freq
    HardResetFrequency = 0

    # Paths where messages are stored
    inboxpath = /var/spool/gammu/inbox/
    outboxpath = /var/spool/gammu/outbox/
    sentsmspath = /var/spool/gammu/sent/
    errorsmspath = /var/spool/gammu/error/

    RunOnReceive = /root/G1SMS/sms_received.sh

Aide: https://wammu.eu/docs/manual/smsd/config.html

## Installer les dépendances Silkaj
Le code de Silkaj (0.5.0) a été inclu et modifié pour accéder aux données brutes (chercher **# G1SMS::** pour trouver les modifications)
https://git.duniter.org/clients/python/silkaj

    Penser à ajouter les dépendances:
    apt-get install python3-pip
    pip3 install setuptools
    pip3 install -r requirements.txt

## CONFIGURATION et LOCALISATION
G1SMS est configuré pour les téléphones français: COUNTRY="+33"
- Traduire et changer ce code dans **./shell/init.sh** pour un autre pays!!
- Modifier dans **./shell/init.sh** le facteur en cours (G1/DU): DUFACTOR=10.04 et le numéro de votre SIM
- Mettre vos clefs Provider de service G1SMS dans les fichiers **g1sms.priv.key** et **g1sms.pub.key**

> RAPPEL: 1 DU = 100 LOVE = DUFACTOR * G1

## TODO

- Make Smartphone / OldPhone different TEXTS
- Backup Sync System & key files

- Add New commands:
  - N UID: register member wallet with current phone wallet
  - MAX 999: Set maximum wallet ammout (unit) => over sent daily/weekly toi member wallet
  - ALIAS xxx cmd: create (xxx) alias for personalised command (cmd)
  - LOOP cmd: run command on each payments
  - CLOSE Close account & Full transfer to another (phone steal or number change)
  
- Add minimum time between Payements (for Blockchain sync?)
- Synchronise phone accounts with multiple G1SMS servers
- Register as Cesium+ service provider
- ...

===
Authors:
- Fred (support@qo-op.com)
- if you participate, please add your name here 
- Version: 0.2
- License: [AGPL-3.0](https://choosealicense.com/licenses/agpl-3.0/)
- Code with Love & Music from https://play.zicmama.com/

