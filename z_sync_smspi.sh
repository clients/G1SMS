#!/bin/bash
echo "RSYNC code to smspi"
rsync -avz --progress --cvs-exclude --exclude "*.key" --exclude "+33*" --exclude "__pycache__" ~/workspace/G1SMS/ -e ssh root@smspi:/root/G1SMS/
