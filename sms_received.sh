#!/bin/bash
################################################################################
# Author: Fred (support@qo-op.com)
# Version: 0.1
# License: AGPL-3.0 (https://choosealicense.com/licenses/agpl-3.0/)
################################################################################
source /root/G1SMS/shell/init.sh
source /root/G1SMS/shell/functions.sh

#SECURITY
security

# Get global variables from gammu-smsd
PHONE="$SMS_1_NUMBER"
TEXT="$SMS_1_TEXT"

log "##################################"
log "$(date)"
log "SMS received $PHONE $TEXT"

# No Reply to MySelf (or enjoy SMS LOOP of the death)
if [ "$PHONE" == "$MASTERPHONE" ]; then exit; fi
if [ "$PHONE" == "Orange Info" ]; then exit; fi

# Extract Received Command (if new update sms_AIDE.sh)
CMD=$(echo "$TEXT" | cut -d ' ' -f 1)
log "COMMAND $CMD"

# Handle commands
case "$CMD" in
	A)
		./shell/sms_AIDE.sh "$PHONE" &
		;;
	N|D)
		MEMBERUID=$(echo "$TEXT" | cut -d ' ' -f 2)
		./shell/sms_NEW.sh "$PHONE" "$MEMBERUID" &
		;;
	P)
		DEST=$(echo "$TEXT" | cut -d ' ' -f 2)
		LOVE=$(echo "$TEXT" | cut -d ' ' -f 3)
		# PI EasterEgg. Any 3.14 amount is G1 Unit
		if [ "$LOVE" == "3.14" ]; then
			LOVE=$(bc -l <<< "scale=0; $LOVE * 100 / $DUFACTOR")
		fi
		checkdest=$(echo "$DEST" | grep -E "^\-?[0-9]+$") # Test if number
		checklove=$(echo "$LOVE" | grep -E "^\-?[0-9]+$") # Test number
		if [[ ${#DEST} == 10 && "$checkdest" != '' && "$checklove" != '' ]]; then
	        ./shell/sms_PAY.sh "$PHONE" "$DEST" "$LOVE" &
		else
			./shell/sms_ERROR.sh "$PHONE" "$TEXT" &
		fi
        ;;
	C)
		./shell/sms_COUNT.sh "$PHONE" &
		;;
	RIB)
    	NAME=$(echo "$TEXT" | cut -d ' ' -f 2)
		./shell/sms_SETRIB.sh "$PHONE" "$NAME" &
		;;
	U)
    	UNIT=$(echo "$TEXT" | cut -d ' ' -f 2)
		./shell/sms_SETUNIT.sh "$PHONE" "$UNIT" &
		;;
	Delivered|Pending|Failed)
		# If delivered/pending notification come back (gammu/phone config)
		exit
		;;
	*)
		./shell/sms_ERROR.sh "$PHONE" "$TEXT" &
		;;
esac

exit

