#!/bin/bash
source /root/G1SMS/shell/init.sh
source /root/G1SMS/shell/functions.sh

if [ -d "/root/G1SMS" ]
then
	cd "/root/G1SMS"
else
	exit
fi

TOTAL=0
TOTALG1=0
NB=0

for f in $(ls ./wallets/ -t); do
	PHONE="$f"
	PIN=$(cat /root/G1SMS/wallets/$PHONE/$PHONE.pin|xargs)
	WUID=""
	if [ -f "/root/G1SMS/wallets/$PHONE/$PHONE.uid" ]; then
		WUID=$(cat "/root/G1SMS/wallets/$PHONE/$PHONE.uid"|xargs)
	fi
	PUBKEY=$(cat "/root/G1SMS/wallets/$PHONE/$PHONE.pub"|xargs)
	AMOUNTG1=$(./silkaj/silkaj amount --auth-scrypt -salt="$PHONE" -password="$PIN")
	echo "-------------------------------"
	echo "$PHONE / $PUBKEY"
	echo "-> $WUID ?"
	echo "Solde ($PHONE) : $AMOUNTG1 G1"
	
	testmin=$( echo "${AMOUNTG1} < ${LIMIT}" | bc -l )
	if [[ "$testmin" -eq "1" ]]; then echo "ALERT!!!"; fi
	
	NB=$(bc <<< "$NB + 1")
	TOTALG1=$(bc -l <<< "$AMOUNTG1 + $TOTALG1")
	AMOUNTLOVE=$(bc <<< "$AMOUNTG1 * 100 / $DUFACTOR")
	TOTAL=$(bc -l <<< "$AMOUNTLOVE + $TOTAL")

# Use G1SMS with phone
#	export SMS_1_NUMBER="$PHONE"
#	export SMS_1_TEXT="N"
#	$(/root/G1SMS/sms_received.sh)

# SEND SMS TO phone
#dest="$PHONE"
#mess="[G1SMS] bonjour, vous avez un porte-monnaie Libre ($AMOUNTLOVE LOVE). Envoyez N pour le conserver. 1l0v3. https://qo-op.com"	
#gammu-smsd-inject TEXT "$dest" -text "$mess"
#sleep 30

done
echo "Nb Porte-Monnaie Libre $NB"
echo "Solde TOTAL : $TOTAL LOVE"
echo "              $TOTALG1 G1"

