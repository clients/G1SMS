#!/bin/bash
echo "Trash Testeur des Scripts G1SMS ('./sms_test.sh help' => Aide)"
echo "##############################################################"

if [[ $1 == "help" ]]; then
echo "'help'
Ce testeur permet de tester les scripts G1SMS sans déclencher l'envoi de SMS ni d'interactions avec la blockchain Duniter Ḡ1"
echo "####################################################"
echo "Surveiller l’exécution : 'tail -f /tmp/g1sms.log'"
echo "####################################################"
echo "IMPORTANT! Après utilisation, ne pas oublier de :
1. Restaurer lien Silkaj en répondant NON, ou manuellement: 
rm ./silkaj/silkaj && ln -s src/silkaj.py ./silkaj/silkaj
2. Restaurer l'envoi de SMS en commentant la fonction 'gammu-smsd-inject' dans './shell/functions.sh'
3. Adapter les valeurs de './shell/init.sh' pour passer en production

export SMS_1_NUMBER='+33600000000'
export SMS_1_TEXT='N'
./sms_received.sh 

HAPPY G1SMS"
exit
fi


echo "Désactiver Silkaj?? O (O/N)"
read oui
if [[ $oui == "N" ]]; then $(rm ./silkaj/silkaj && ln -s src/silkaj.py ./silkaj/silkaj);
else $(rm ./silkaj/silkaj && ln -s ../fake_silkaj.sh ./silkaj/silkaj); fi

# Define default numbers, randomly choosen
arr[0]="+33600000000"
arr[1]="+33612345678"
rand=$[$RANDOM % ${#arr[@]}]
num=${arr[$rand]}

rand2=$[$RANDOM % ${#arr[@]}]
num2=${arr[$rand2]}
num2="0${num2:3:10}"

# Ask for phone number
echo "Numero ( $num ) ? "
read SMS_1_NUMBER
if [[ $SMS_1_NUMBER == "" ]]; then SMS_1_NUMBER="$num"; fi
export SMS_1_NUMBER

# Ask for SMS text
echo "SMS ( A - N - C - U G1 - P $num2 100 ... )? A"
read SMS_1_TEXT
if [[ $SMS_1_TEXT == "" ]]; then SMS_1_TEXT="A"; fi
export SMS_1_TEXT

$(./sms_received.sh)


