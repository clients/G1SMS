#!/bin/bash
################################################################################
# Author: Fred (support@qo-op.com)
# Version: 0.1
# License: AGPL-3.0 (https://choosealicense.com/licenses/agpl-3.0/)
################################################################################
source /root/G1SMS/shell/init.sh
source /root/G1SMS/shell/functions.sh
log "sms_AIDE.sh ($1=phone)"

MESS="[Aide]
'N' => Nouveau porte-monnaie
'U LOVE/G1/DU' => Unité
'C' => Solde
'P 06xxxxxxxx NNN' => Virer NNN à 06xxxxxxxx
https://qo-op.com/aide"

# Send response SMS
sms_SEND "$1" "$MESS"

MESS="Votre porte-monnaie contient de la Monnaie Libre (G1).
Devenez membre et co-produisez votre Dividende Universel!
https://qo-op.com"
# Send response SMS
sms_SEND "$1" "$MESS"

exit
