#!/bin/bash
# Adapter ces valeurs au contexte spatio-temporel du Service G1SMS
##################################################################
# Acivate logging to /tmp/g1sms.log (YES/NO)
DOLOG="YES" 

###############################################################
# Each account is refered to caller phone international writing
COUNTRY="+33"
####################################################################
# DU has a G1 value changed every 6 month! Needs to be ugraded there
DUFACTOR=10.04
##############################
# Minimal 
LIMIT=2 # Solde minimum = 2 G1
COMMISSION=1 # transaction commission amount (G1) 

###########################################
MASTERPHONE=+33666805720
export MASTERPUB=$(cat "/root/G1SMS/g1sms.pub.key")
export MASTERKEYFILE="/root/G1SMS/g1sms.priv.key"
