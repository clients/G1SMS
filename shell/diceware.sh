#!/usr/bin/env bash
# Default to 6 word passphrase
WORDCOUNT=${1-4}
# Download the wordlist
# wget -nc -O ~/.diceware-wordlist http://world.std.com/%7Ereinhold/diceware.wordlist.asc 2> /dev/null
# print a list of the diceware words
cat /root/G1SMS/shell/diceware-wordlist.txt |
awk '/[1-6][1-6][1-6][1-6][1-6]/{ print $2 }' |
# randomize the list order
shuf --random-source=/dev/urandom |
# pick the first n words
head -n ${WORDCOUNT} |
# pretty print
tr '\n' ' '
echo
