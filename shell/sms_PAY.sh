#!/bin/bash
################################################################################
# Author: Fred (support@qo-op.com)
# Version: 0.1
# License: AGPL-3.0 (https://choosealicense.com/licenses/agpl-3.0/)
################################################################################
source /root/G1SMS/shell/init.sh
source /root/G1SMS/shell/functions.sh
log "X sms_PAY.sh ($1=phone, $2=destphone, $3=amount)"

phone="$1"
phonedest="$2"
amount="$3"

# Initialise PHONE, PIN, PUBKEY, UNIT, HIST
sms_INIT_ACCOUNT "$phone" "NOSMS"
if [[ $UNKNOWN == "unknown" ]]; then
	sms_ERROR "$phone" "Porte-monnaie inconnu. Envoyez N pour le créer."
	exit
fi

pin="$PIN"
pubkey="$PUBKEY"
unit="$UNIT"
hist="$HIST"

# Get AMOUNTG1, AMOUNTLOVE, AMOUNTDU
AMOUNTG1=$(./silkaj/silkaj amount --auth-scrypt -salt="$PHONE" -password="$PIN")
AMOUNTDU=$(bc <<< "scale=3; $AMOUNTG1 / $DUFACTOR")
AMOUNTLOVE=$(bc <<< "$AMOUNTG1 * 100 / $DUFACTOR")

# Handle source unit conversion to VIR(G1)
case "$unit" in
G1)
	VIR="$amount";
	;;
DU)
	VIR=$(bc -l <<< "scale=3; $amount * $DUFACTOR")
	;;
LOVE)
	VIR=$(bc -l <<< "scale=2; $amount * $DUFACTOR / 100")
	unit="Love <3"
	;;
*)
	VIR=$(bc -l <<< "scale=2; $amount * $DUFACTOR / 100")
	unit="Love <3"
	;;
esac

# Need at least $MIN G1 available!
MIN=$(bc -l <<< "$AMOUNTG1 + $LIMIT")
testmin=$( echo "${VIR} < ${MIN}" | bc -l )
log "TEST : $VIR < $MIN ? $testmin"

if [[ "$testmin" -eq "1" ]]; then
	# Add COUNTRY code to phonedest + phonedest INIT
	phonedest="$COUNTRY${phonedest:1:10}"
	sms_INIT_ACCOUNT "$phonedest"
	pindest="$PIN"
	pubkeydest="$PUBKEY"
	unitdest="$UNIT"
	histdest="$HIST"

	# Payement
	PAY=$(./silkaj/silkaj transaction --auth-scrypt -salt="$phone" -password="$pin" --amount="$VIR" --output="$pubkeydest" --comment="[G1SMS] Envoi $amount $unit" -y)
 	# + G1SMS Commission
	COM=$(./silkaj/silkaj transaction --auth-scrypt -salt="$phone" -password="$pin" --amount="$COMMISSION" --output="$MASTERPUB" --comment="[G1SMS] Commission" -y)

	# LOG ACCOUNT HISTORY EVENTS
	log_history $phone "PAY:$phonedest:$VIR"
	log_history $phonedest "RCV:$phone:$VIR"

	# Send response SMS
	mess_src="[G1SMS]
Envoi de $amount $unit vers $phonedest : OK!
+ Commission G1SMS: $COMMISSION G1"
	sms_SEND "$phone" "$mess_src"

	#####################################
	# Adapt VIR to unitdest => RECV
	case "$unitdest" in
	G1)
		RECV="$VIR";
		;;
	DU)
		RECV=$(bc -l <<< "scale=3; $VIR / $DUFACTOR")
		;;
	LOVE)
		RECV=$(bc -l <<< "scale=2; $VIR * 100 / $DUFACTOR")
		unitdest="Love <3"
		;;
	*)
		RECV=$(bc -l <<< "scale=2; $VIR * 100 / $DUFACTOR")
		unitdest="Love <3"
		;;
	esac

	# Send dest SMS
	mess_dest="[G1SMS]
Vous venez de recevoir $RECV $unitdest de la part de $phone
Envoyez A pour des explications."
	sms_SEND "$phonedest" "$mess_dest"

#####################################
# Amount too High, solde insuffisant
else
	text="
Solde: $AMOUNTLOVE LOVE
($AMOUNTG1 G1 = $AMOUNTDU DU)
INSUFFISANT pour virer $amount $UNIT
Rechargez https://cesium.madeinzion.org/api/#/v1/payment/$PUBKEY?amount=100"
	sms_ERROR "$phone" "$text"
fi

exit
