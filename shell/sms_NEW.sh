#!/bin/bash
################################################################################
# Author: Fred (support@qo-op.com)
# Version: 0.1
# License: AGPL-3.0 (https://choosealicense.com/licenses/agpl-3.0/)
################################################################################
source /root/G1SMS/shell/init.sh
source /root/G1SMS/shell/functions.sh
log "X sms_NEW.sh ($1=phone, $2=uid)"

PHONE="$1"
MEMBERUID="$2"

# Initialise PHONE, PIN, PUBKEY, UNIT, HIST
sms_INIT_ACCOUNT $PHONE
if [ "$MEMBERUID" != "" ]; then
	sms_uid2key "$MEMBERUID"
fi
mess="[G1SMS] (https://qo-op.com)
$MEMBERUID Porte-monnaie Libre ouvert.
Id: $PHONE
Code: $PIN

Clef Publique (RIB)
"
sms_SEND "$PHONE" "$mess"

mess="$PUBKEY"
sms_SEND "$PHONE" "$mess"

exit
