#!/bin/bash
################################################################################
# Author: Fred (support@qo-op.com)
# Version: 0.1
# License: AGPL-3.0 (https://choosealicense.com/licenses/agpl-3.0/)
################################################################################
source /root/G1SMS/shell/init.sh
source /root/G1SMS/shell/functions.sh
log "X sms_SETRIB.sh ($1=phone, $2=name)"

phone="$1"
name="$2"
NAMEFILE="./wallets/$phone/$phone.rib"

# Initialise PHONE, PIN, PUBKEY, UNIT, HIST
sms_INIT_ACCOUNT "$phone" "NOSMS"
if [[ $UNKNOWN == "unknown" ]]; then
	sms_ERROR "$phone" "Porte-monnaie inconnu. Envoyez N pour le créer."
	exit
fi

# No name given (return old one or create default)
if [[ $name == "RIB" ]]; then
	if [[ -f "$NAMEFILE" ]]; then
		name=$(cat "$NAMEFILE")
	else
		name="G1SMS_${PUBKEY:0:8}";
	fi
fi

echo "$name" > "$NAMEFILE"

########################
#TODO inform Pod Cesium+
########################

mess="[G1SMS]
Votre porte-monnaie Libre porte le libellé: $name"

sms_SEND "$phone" "$mess"

exit
