#!/bin/bash
################################################################################
# Author: Fred (support@qo-op.com)
# Version: 0.1
# License: AGPL-3.0 (https://choosealicense.com/licenses/agpl-3.0/)
################################################################################
source /root/G1SMS/shell/init.sh
source /root/G1SMS/shell/functions.sh
log "X sms_COUNT.sh ($1=phone $2=NOSMS)"
PHONE=$1

# Initialise PHONE, PIN, PUBKEY, UNIT, HIST
sms_INIT_ACCOUNT "$PHONE" "NOSMS"
if [[ $UNKNOWN == "unknown" ]]; then
	sms_ERROR "$PHONE" "Porte-monnaie inconnu. Envoyez N pour le créer."
	exit
fi

# Check account amount
AMOUNTG1=$(./silkaj/silkaj amount --auth-scrypt -salt="$PHONE" -password="$PIN")
AMOUNTDU=$(bc <<< "scale=2; $AMOUNTG1 / $DUFACTOR")
AMOUNTLOVE=$(bc <<< "$AMOUNTG1 * 100 / $DUFACTOR")

case "$UNIT" in
	G1)
		AMOUNT=$AMOUNTG1
		;;
	DU)
		AMOUNT=$AMOUNTDU
		;;
	*)
		AMOUNT=$AMOUNTLOVE
		;;
esac

# LOG
log "Solde = $AMOUNT ($UNIT)"

# If script control only run: NOSMS
if [ "$2" != "NOSMS" ]; then
	mess="[G1SMS]
Compte: $PHONE
Solde: $AMOUNT $UNIT
"
	# Send response SMS
	sms_SEND "$PHONE" "$mess"
else
	echo $AMOUNTG1
fi

exit
