#!/bin/bash
################################################################################
# Author: Fred (support@qo-op.com)
# Version: 0.1
# License: AGPL-3.0 (https://choosealicense.com/licenses/agpl-3.0/)
################################################################################
source /root/G1SMS/shell/init.sh
source /root/G1SMS/shell/functions.sh
log "X sms_SETUNIT.sh ($1=phone, $2=unit)"

phone="$1"
unit="$2"
UNITFILE="./wallets/$phone/$phone.unit"

# Initialise PHONE, PIN, PUBKEY, UNIT, HIST
sms_INIT_ACCOUNT "$phone" "NOSMS"
if [[ $UNKNOWN == "unknown" ]]; then
	sms_ERROR "$phone" "Porte-monnaie inconnu. Envoyez N pour le créer."
	exit
fi

# No unit received
if [[ $unit == "U" ]]; then
	if [[ -f "$UNITFILE" ]]; then
		unit=$(cat "$UNITFILE")
	else
		unit="LOVE";
	fi
fi

echo "$unit" > "$UNITFILE"

mess="[G1SMS]
Unité de votre porte-monnaie Libre: $unit"

sms_SEND "$phone" "$mess"

exit
