#!/bin/bash
# G1SMS COMMUN FUNCTIONS

# !!!!!!!!!!! COMMENT / UNCOMMENT to activate/desactivate real SMS
#function gammu-smsd-inject () {  ###
#	echo ">>> sending SMS $4 TO $2 " >> /tmp/g1sms.log  ###
#} ###

function security () {
if [ -d "/root/G1SMS" ]
then
	cd "/root/G1SMS"
else
	echo "G1SMS MUST BE INSTALLED IN HOME ROOT DIRECTORY. SET YOUR WALLET KEYS! chmod 700 ./wallets"
	exit
fi

chown -R root:root ./wallets
chmod 700 ./wallets

}

function choose_peer (){
# TODO: Diverse Duniter access
# https://gitlab.com/zicmama/tgen
# peers.txt: the peers you want to use, one per line, you can add a # at the beginning of the line to temporarily stop using a peer,
	nbpeers=`grep -c -v "#" peers.txt`
	peernum=$(( $RANDOM % $nbpeers + 1 ))
	peer=`grep -v "#" peers.txt|awk '{if(NR==n)print $0}' n=$peernum`
	echo $peer
}

function log () {
# log ($1=text)
	if [ "$DOLOG" == "YES" ]
	then
		echo "$1" >> /tmp/g1sms.log
	fi
}

function sms_SEND () {
# sms_SEND ($1=phone, $2=message)
	local dest="$1"
	local mess="$2"
	
	gammu-smsd-inject TEXT "$dest" -text "$mess"
}

function sms_ERROR () {
# sms_ERROR ($1=phone, $2=message)
	local dest="$1"
	local mess="[G1SMS] Erreur
$2
Envoyez A pour de l'Aide."
	
	gammu-smsd-inject TEXT "$dest" -text "$mess"
}

function log_history () {
log "history ($1=phone, $2=message)"
	PHONE="$1"
	HISTFILE="./wallets/$PHONE/$PHONE.hist"

	echo "$(date +%Y%m%d-%H:%M:$S)_$2" >> "$HISTFILE"
	
}

function sms_uid2key (){
	UIDFILE="./wallets/$PHONE/$PHONE.uid"
	if [ -f "$UIDFILE" ]; then
		UIDPUBKEY=$(cat "$UIDFILE")
	else
	if [ "$1" != "" ]; then
		if [[ "$UIDPUBKEY" == "" && "$1" != "" ]]; then
			UIDPUBKEY=$(./silkaj/silkaj id "$1" | grep -w "$1" | awk '{print $2}')
				if [ "$UIDPUBKEY" != "" ]; then
					log "NEWUID:$1:$UIDPUBKEY"
					INIT=$(./silkaj/silkaj transaction --auth-scrypt -salt="$PHONE" -password="$PIN" --amount=0.1 --output=$UIDPUBKEY --comment="THIRD_PARTY_MANAGER:G1SMS:$MASTERPUB" -y)		
					echo "$UIDPUBKEY" > "$UIDFILE"
				fi
			fi
		fi
	fi
}

#######################################"
function sms_INIT_ACCOUNT () {
log "sms_INIT_ACCOUNT ($1=phone, $2=NOSMS)"
	PHONE="$1"
	UNKNOWN=0
	# Initiate PHONE settings files and values
	PINFILE="./wallets/$PHONE/$PHONE.pin" # Contains phone wallet diceware password
	PUBKEYFILE="./wallets/$PHONE/$PHONE.pub" # Contains phone wallet public key (RIB)
	UNITFILE="./wallets/$PHONE/$PHONE.unit" # Contains phone wallet prefered unit (LOVE,G1,DU)
	HISTFILE="./wallets/$PHONE/$PHONE.hist" # Contains phone wallet history (see log_history function)
	UIDFILE="./wallets/$PHONE/$PHONE.uid" # Contains phone wallet related member UID (n)
	
	# (NO PIN) FirstAccount Create wallet 
	if [ ! -f "$PINFILE" ]; then
		if [[ $2 == "NOSMS" ]]; then UNKNOWN="unknown"; return; fi
		mkdir -p "./wallets/$PHONE/"
		
		# Create Account Files (Get back old beta account)
		if [ -f "./keys/$PHONE.pin" ]; then ###
			mv "./keys/$PHONE.pin" "$PINFILE"; ###
		else ###
			PIN=$(./shell/diceware.sh | xargs)
			PUBKEY=$(./silkaj/silkaj generate_auth_file --auth-scrypt -salt="$PHONE" -password="$PIN")
			
			# Send first G1SMS transaction to activate account (THIRD_PARTY_MANAGER no member wallet)
			# log "$MASTERKEYFILE $PUBKEY THIRD_PARTY_MANAGER:$MASTERPUB"
			INIT=$(./silkaj/silkaj transaction --auth-file -file="$MASTERKEYFILE" --amount=2.01 --output=$PUBKEY --comment="THIRD_PARTY_MANAGER:$MASTERPUB" -y)
			#log "Transaction1 $INIT"
			DOUBLE=$(./silkaj/silkaj transaction --auth-scrypt -salt="$PHONE" -password="$PIN" --amount=1 --output=$MASTERPUB --comment="THIRD_PARTY_MANAGER:$MASTERPUB:ACK" -y)
			#log "Transaction2 $DOUBLE"
			echo "$PIN" > "$PINFILE"
		fi ###
		if [ -f "./keys/$PHONE.pub" ]; then ###
			mv "./keys/$PHONE.pub" "$PUBKEYFILE"; ###
		else ###
			echo "$PUBKEY" > "$PUBKEYFILE"
		fi ###
		echo "LOVE" > "$UNITFILE"
		echo "$(date +%Y%m%d-%H:%M): OUVERTURE" >> "$HISTFILE"
	fi

	# Init caller : GLOBAL VAR SET
	PIN=$(cat "$PINFILE" | xargs)
	PUBKEY=$(cat "$PUBKEYFILE" | xargs)
	UNIT=$(cat "$UNITFILE" | xargs)
	HIST=$(tail -n3 "$HISTFILE")
	
	# LOG
	log "PHONE: $PHONE"
	log "PIN: $PIN"
	log "KEY: $PUBKEY"	
	log "UNIT: $UNIT"	
	log "HIST: $HIST"	
	
}

