#!/bin/bash
################################################################################
# Author: Fred (support@qo-op.com)
# Version: 0.1
# License: AGPL-3.0 (https://choosealicense.com/licenses/agpl-3.0/)
################################################################################
source /root/G1SMS/shell/init.sh
source /root/G1SMS/shell/functions.sh
log "X sms_ERROR.sh ($1=phone, $2=message)"

MESS="Commande inconnue $2."

# Send response SMS
$(sms_ERROR "$1" "$MESS")

exit
